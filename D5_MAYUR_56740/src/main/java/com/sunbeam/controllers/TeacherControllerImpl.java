package com.sunbeam.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sunbeam.pojos.Credentials;
import com.sunbeam.pojos.Teacher;
import com.sunbeam.services.TeacherServiceImpl;
@Controller
public class TeacherControllerImpl {
private TeacherServiceImpl teacherService;

public TeacherControllerImpl(TeacherServiceImpl teacherService) {
	super();
	this.teacherService = teacherService;
}
@RequestMapping("/index") 
public String index() {
	
	return "index";}

@RequestMapping("/login") 
public String login(Model model) {
	Credentials cred=new Credentials("your@gmail.com","");
	model.addAttribute("command", cred);
	return "login";
}
@RequestMapping("/validate")
public String validate(Credentials cred,HttpSession session) {
	Teacher teacher = teacherService.authenticateUser(cred.getEmail(), cred.getPassword());
	if(teacher == null)
		return "failed";
	session.setAttribute("teacher", teacher);
	if(teacher.getRoll().equals("admin"))
		return "redirect:manage";
	return "redirect:details";
}

@RequestMapping("/details")
public String details(String email,Model model,HttpSession session) {
	Teacher teacher = teacherService.findByEmail(email);
	model.addAttribute("teacher",teacher);
	return "details"; 
}
@GetMapping("/edit")
public String edit(@RequestParam("id") int teacherId,Model model) {
	Teacher teacher=teacherService.findById(teacherId);
	model.addAttribute("teacher",teacher);
	return "edit";
}

@PostMapping("/edit")
public String update(Teacher teacher) {
	Teacher u=	teacherService.saveTeacher(teacher);
	return"edit";
	
	
}
@RequestMapping("/manage")
public String manage(Model model) {
	List<Teacher> list = teacherService.findAllTeachers();
	model.addAttribute("teacherList", list);
	return "manage"; // --> manage.jsp
}
@RequestMapping("/delete")
public String delete(Teacher u,Model model) {
	teacherService.deleteTeacher(u);
	model.addAttribute("teacher" );
	return "redirect:manage"; // --> manage.jsp


}

}
