package com.sunbeam.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.pojos.Subject;

public interface SubjectDao extends JpaRepository<Subject, Integer>{

}
