package com.sunbeam.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.pojos.Teacher;

public interface TeacherDao extends JpaRepository<Teacher, Integer>{
	Teacher findByEmail(String email);
}
