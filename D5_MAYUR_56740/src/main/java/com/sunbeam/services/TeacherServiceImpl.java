package com.sunbeam.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.sunbeam.daos.TeacherDao;
import com.sunbeam.pojos.Teacher;

@Transactional
@Service
public class TeacherServiceImpl {
private TeacherDao teacherDao;

public TeacherServiceImpl(TeacherDao teacherDao) {
	super();
	this.teacherDao = teacherDao;
}

public Teacher findByEmail(String email) {
	return teacherDao.findByEmail(email);
}

public Teacher authenticateUser(String email, String password) {
	Teacher teacher = teacherDao.findByEmail(email);
	if(teacher != null && teacher.getPassword().equals(password))
		return teacher;
	return null;
}
public Teacher findById(int id) {
	Optional<Teacher>b=teacherDao.findById(id);
	return b.orElse(null);
}
public Teacher saveTeacher(Teacher u) {
	return teacherDao.save(u);
}
public List<Teacher> findAllTeachers() {
	return teacherDao.findAll();
}
public void deleteTeacher(Teacher teacher) {
teacherDao.delete(teacher);
}
}
