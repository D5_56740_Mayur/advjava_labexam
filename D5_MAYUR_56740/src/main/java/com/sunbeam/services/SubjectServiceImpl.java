package com.sunbeam.services;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.sunbeam.daos.SubjectDao;
import com.sunbeam.pojos.Subject;
import com.sunbeam.pojos.Teacher;
@Transactional
@Service
public class SubjectServiceImpl {
 private SubjectDao subjectDao;

public SubjectServiceImpl(SubjectDao subjectDao) {
	super();
	this.subjectDao = subjectDao;
}
 
public Subject saveSubject(Subject s) {
	return subjectDao.save(s);
}
}
