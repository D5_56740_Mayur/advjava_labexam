package com.sunbeam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class D5Mayur56740Application {

	public static void main(String[] args) {
		SpringApplication.run(D5Mayur56740Application.class, args);
	}

}
