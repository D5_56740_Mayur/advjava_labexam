package com.sunbeam.pojos;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="tb_teacher")
public class Teacher {
	@Id
	@Column(name="id")
private int id;
	@Column(name="t_name")
private String name;
	@Column(name="t_password")
private String password;
	@Column(name="t_email")
private String email;
	@Column(name="t_dob")
private Date dob;
	@Column(name="t_roll")
private String roll;

public Teacher() {
	// TODO Auto-generated constructor stub
}

public Teacher(int id, String name, String password, String email, Date dob, String roll) {
	super();
	this.id = id;
	this.name = name;
	this.password = password;
	this.email = email;
	this.dob = dob;
	this.roll = roll;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public Date getDob() {
	return dob;
}

public void setDob(Date dob) {
	this.dob = dob;
}

public String getRoll() {
	return roll;
}

public void setRoll(String roll) {
	this.roll = roll;
}

@Override
public String toString() {
	return "Teacher [id=" + id + ", name=" + name + ", password=" + password + ", email=" + email + ", dob=" + dob
			+ ", roll=" + roll + "]";
}

}
