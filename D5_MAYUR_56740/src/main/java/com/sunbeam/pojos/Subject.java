package com.sunbeam.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="tb_subjects")
public class Subject {
	@Id
	@Column(name="s_id")
private int id;
	@Column(name="s_name")
	private String name;
	@Column(name="s_duration")
	private String duration;
	@Column(name="t_course")
	private String course;
	@Column(name="t_id")
	private int t_id;
	
	public Subject() {
		// TODO Auto-generated constructor stub
	}

	public Subject(int id, String name, String duration, String course, int t_id) {
		super();
		this.id = id;
		this.name = name;
		this.duration = duration;
		this.course = course;
		this.t_id = t_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public int getT_id() {
		return t_id;
	}

	public void setT_id(int t_id) {
		this.t_id = t_id;
	}
	
	
}
