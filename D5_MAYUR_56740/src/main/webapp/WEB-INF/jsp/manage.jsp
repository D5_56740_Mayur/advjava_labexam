<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>Teachers List</h2>
<table border="1"colour="red">
		<thead>
			<tr>
				<td>Id</td>
				<td>Name</td>
				<td>Password</td>
				<td>Email</td>
				<td>DOB</td>
				<td>Roll</td>
				
				
				<td>Action</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="teacher" items="${teacherList}">
				<tr>
					<td>${teacher.id}</td>
					<td>${teacher.name}</td>
					<td>${teacher.password}</td>
					<td>${teacher.email}</td>
					<td>${teacher.dob}</td>
					<td>${teacher.roll}</td>
					
					
					<td>
						<a href="/delete?id=${vendor.id}">Delete</a>				
					</td>
				</tr>				
			</c:forEach>
		</tbody>
		</table>
		<a href="/registration">Add New Vendor</a>
</body>
</html>