<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>Update Your Details</h2>
	<sf:form modelAttribute="teacher" method="post" action="/edit">
		<table>
			
		<tr>
				<td>Id</td>
				<td><sf:input path="id" readonly="true"/>
				<td></td>
			</tr>
			<tr>
				<td>Name</td>
				<td><sf:input path="name" />
				<td></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><sf:input path="email" readonly="true"/>
				<td></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><sf:input path="password"/>
				<td></td>
			</tr>
			<tr>
				<td>DOB</td>
				<td><sf:input path="dob" readonly="true"/>
				<td></td>
			</tr>
			
			<tr>
				<td>Role</td>
				<td><sf:input path="role" readonly="true"/>
				<td></td>
			</tr>
			
			<tr>
				<td></td>
				<td><input type="submit" value="Update"/></td>
				<td></td>
			</tr>
		</table>
	</sf:form>	
	<hr/>
		<a href="/details">Return</a>
</body>
</html>