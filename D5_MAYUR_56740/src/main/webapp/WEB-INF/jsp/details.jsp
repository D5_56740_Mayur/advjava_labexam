<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Details</title>
</head>
<body>
	Hello, ${teacher.name} <hr/>
	
	<h2>Your Details</h2>
	<h3>ID: ${teacher.id}</h3>
	<h3>NAME: ${teacher.name}</h3>
	<h3>EMAIL: ${teacher.email}</h3>
	<h3>PASSWORD: ${teacher.password}</h3>
	<h3>DOB ${teacher.dob}</h3>
	<h3>YOUR ROLE: ${teacher.roll}</h3>
	
<a href="/edit">Edit your Details</a>
<a href="/add">Add Book</a>
	<a href="/logout">Sign Out</a>
</body>
</html>